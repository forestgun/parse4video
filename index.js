// Example express application adding the parse-server module to expose Parse
// compatible API routes.
require('newrelic');

var express = require('express');
var ParseServer = require('parse-server').ParseServer;

var S3Adapter = require('parse-server-s3-adapter');


var path = require('path');

var databaseUri = process.env.DATABASE_URI || process.env.MONGODB_URI;

if (!databaseUri) {
  console.log('DATABASE_URI not specified, falling back to localhost.');
}

var s3Adapter = new S3Adapter('AKIAIWNNOJO2CDM25SQA',
                  '9FaZjwPJlHyvmymErzsbyv01O1XuatGjNQUbbCE8', 'padel4video403015pp', {
                    region: 'eu-west-1',
                    bucketPrefix: '',
                    directAccess: false,
                    baseUrl: '',
                    signatureVersion: 'v4',
                    globalCacheControl: 'public, max-age=86400'  // 24 hrs Cache-Control.
                  });

var api = new ParseServer({
  databaseURI: databaseUri || 'mongodb://localhost:27017/dev',
  cloud: process.env.CLOUD_CODE_MAIN || __dirname + '/cloud/main.js',
  appId: process.env.APP_ID || 'myAppId',
  masterKey: process.env.MASTER_KEY || '', //Add your master key here. Keep it secret!
  serverURL: process.env.SERVER_URL || 'http://localhost:1337/parse',  // Don't forget to change to https if needed
  clientKey: process.env.CLIENT_KEY || '',
  javascriptKey: process.env.JAVASCRIPT_KEY || '',
  restAPIKey: process.env.RESTAPI_KEY || '',
  dotNetKey: process.env.DOTNET_KEY || '',
  liveQuery: {
    classNames: ["User", "Comments"], // List of classes to support for query subscriptions
    redisURL: process.env.REDISCLOUD_URL   // Redis para LiveQuery
  },
  allowClientClassCreation: false,    // - Set to false to disable client class creation. Defaults to true.
  enableAnonymousUsers: false,        // - Set to false to disable anonymous users. Defaults to true.

 
  // Enable email verification
  //verifyUserEmails: true,
  // set preventLoginWithUnverifiedEmail to false to allow user to login without verifying their email
  // set preventLoginWithUnverifiedEmail to true to prevent user from login if their email is not verified
  preventLoginWithUnverifiedEmail: true, // defaults to false
  // The public URL of your app.
  // This will appear in the link that is used to verify email addresses and reset passwords.
  // Set the mount path as it is in serverURL
  publicServerURL:  process.env.SERVER_URL || 'http://localhost:1337/parse',  // Don't forget to change to https if needed
  // Your apps name. This will appear in the subject and body of the emails that are sent.
  appName: 'Padel4Video App',


 // FILE STATIC HOSTING AWS S3
 filesAdapter: s3Adapter




});
// Client-keys like the javascript key or the .NET key are not necessary with parse-server
// If you wish you require them, you can set them as options in the initialization above:
// javascriptKey, restAPIKey, dotNetKey, clientKey

var app = express();

// Serve static assets from the /public folder
app.use('/public', express.static(path.join(__dirname, '/public')));

// Serve the Parse API on the /parse URL prefix
var mountPath = process.env.PARSE_MOUNT || '/parse';
app.use(mountPath, api);

// Parse Server plays nicely with the rest of your web routes
app.get('/', function(req, res) {
  res.status(200).send('I dream of being a website.  Please star the parse-server repo on GitHub!');
});

// There will be a test page available on the /test path of your server url
// Remove this before launching your app
app.get('/test', function(req, res) {
  res.sendFile(path.join(__dirname, '/public/test.html'));
});

var port = process.env.PORT || 1337;
var httpServer = require('http').createServer(app);
httpServer.listen(port, function() {
    console.log('parse-server-example running on port ' + port + '.');
});

// This will enable the Live Query real-time server
//ParseServer.createLiveQueryServer(httpServer);
// This will enable the Live Query real-time server
var parseLiveQueryServer = ParseServer.createLiveQueryServer(httpServer, {

  appId: process.env.APP_ID || 'myAppId',
  masterKey: process.env.MASTER_KEY || '', //Add your master key here. Keep it secret!
  keyPairs: {
    "clientKey": process.env.CLIENT_KEY || '',
    "javascriptKey": process.env.JAVASCRIPT_KEY || '',
    "restAPIKey": process.env.RESTAPI_KEY || '',
    "dotNetKey": process.env.DOTNET_KEY || '',
    "windowsKey": "",
    "masterKey": process.env.MASTER_KEY || ''
  },
  serverURL: process.env.SERVER_URL || 'http://localhost:1337/parse',  // Don't forget to change to https if needed
  websocketTimeout: 10 * 1000,
  cacheTimeout: 60 * 60 * 1000,
  logLevel: 'VERBOSE' , //,
  redisURL: process.env.REDISCLOUD_URL   // Redis para LiveQuery

});
